# https://github.com/p-doyle/Python-KasaSmartPowerStrip
import threading
import socket
import sys

import time
from KasaSmartPowerStrip import SmartPowerStrip

import kasa
import asyncio
from kasa import Discover
import RPi.GPIO as GPIO

waveengine_normal_mode_status_pin = 24;     #IO24, pin 5
GPIO.setmode(GPIO.BCM)
GPIO.setup(waveengine_normal_mode_status_pin, GPIO.IN)

filter_roller_device_alias = "Filter Roller"
power_strip_model = "HS300(US)"

def monitor_strip_thread_func(ip_address):
  print("Starting monitoring for: %s" % ( ip_address ) )
  time.sleep(0.5)
  
  errors = 0
  
  while( errors < 5 ):
    power_strip = SmartPowerStrip(ip_address)
    model = power_strip.get_system_info()['system']['get_sysinfo']['model']
    if (model==power_strip_model):
      for plug_idx in [1,2,3,4,5,6]:
        try:
          # Without some idle time, the strips are very slow to update/respond to cloud messages
          time.sleep(1)

          # 'state' wasn't updating with a presistant SmartPowerStrip() instance
          # Creating a new one every loop solved the problem
          power_strip = SmartPowerStrip(ip_address)
          plug_info = power_strip.get_plug_info(plug_num=plug_idx)[0]      # Does a copy, not actually polling device
          if ( plug_info['alias'] == filter_roller_device_alias ):
            print("Found roller!  IP: " + ip_address + ", Plug index: " + str(plug_idx) )
            try:
              while(1):
                power_strip = SmartPowerStrip(ip_address)
                roller_on = power_strip.get_plug_info(plug_num=plug_idx)[0]['state']
                waveengine_in_normal_mode = GPIO.input(waveengine_normal_mode_status_pin)
                if (roller_on and not(waveengine_in_normal_mode)):
                   power_strip.toggle_plug('off', plug_name=filter_roller_device_alias)
                   print("Roller off")
                elif (not(roller_on) and waveengine_in_normal_mode):
                   # Delay filter roller turn on by 20 seconds to allow the pumps to clear the sump
                   time.sleep(20)
                   power_strip.toggle_plug('on', plug_name=filter_roller_device_alias)
                   print("Roller on")
                else:
                 time.sleep(1)

            except Exception as e:
              print("Unhandled error during filter roller commanding: " + str(e))
          errors = 0
        except socket.timeout:
          errors += 1
        except Exception as e:
          print("Unhandled Error during plug update: " + str(e))
          errors += 1
    time.sleep(30)
  
print("Sump controller service started!")
threads = {}
while(1):
  try:
    kasa_devices = asyncio.run(Discover.discover())
    for ip_address, kasa_dev in kasa_devices.items():
      asyncio.run(kasa_dev.update())
      if not (ip_address in threads):
        print("New device discovery: %s @ %s" % ( kasa_dev.alias, ip_address))
        threads[ip_address] = threading.Thread(target=monitor_strip_thread_func, args=[ip_address])
        threads[ip_address].start()

    # We can't iterate over threads directly as we're deleting elements and python doesn't like that done in the loop
    for ip_address in list(threads.keys()):
      if (not threads[ip_address].is_alive()):
        print("Thread for %s dead, removing" % ( ip_address ) )
        del threads[ip_address]
  except Exception as e:
    print("Unhandled error during discovery: " + str(e))

  time.sleep(5)