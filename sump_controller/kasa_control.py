# pip3 install pyinstaller
# pyinstaller -F kasa_control.py
# sudo mv ./dist/kasa_control /usr/local/bin/

import os
import socket
import sys, getopt
from KasaSmartPowerStrip import SmartPowerStrip

import kasa
import asyncio
from kasa import Discover

optStatus = False
optDrive  = False
optList   = False
ip_address = ''
plug_number = -1
model_target = "HS300"
plug_cmd_state = ''
printed_plugs = []

def print_usage():
  print('Arguments: ')
  print('\t-h              : Prints this help message')
  print('\t-l              : List Kasa devices')
  print('\t-i <ip address> : strip ip address')
  print('\t-p <plug>       : plug # on strip')
  print('\t-s              : get a plug\'s status')
  print('\t-d <on/off>     : turn a plug on or off')
  print('\tExample: python kasa_control.py -m HS300 -i 192.168.0.220 -p 4 -s')

def parse_args(argv):
  global optStatus
  global optDrive
  global optList
  global ip_address
  global plug_number
  global plug_cmd_state

  try:
    opts, args = getopt.getopt(argv, 'ld:i:p:sh')

  except getopt.GetoptError:
    print('Error parsing arguments.  Required usage:')
    print_usage()
    sys.exit(2)

  if len(opts) == 0:
    print_usage()
    sys.exit()

  for opt, arg in opts:
    if opt == '-h':
      print_usage()
      sys.exit()

    elif opt == "-i":
      ip_address = arg

    elif opt == '-p':
      plug_number = int(arg)

    elif opt == "-l":
      optList = True

    elif opt == "-d":
      optDrive = True
      if (arg=="on" or arg=="off"):
        plug_cmd_state = arg
      else:
        print_usage()
        sys.exit(2)

    elif opt == "-s":
      optStatus = True
      
def list_devices(ip_address):
  global printed_plugs
  global model_target
  power_strip = SmartPowerStrip(ip_address)
  power_strip_info = power_strip.get_system_info()
  model = power_strip_info['system']['get_sysinfo']['model']
  if ( model.find(model_target) > -1):
    if ('children' in power_strip_info['system']['get_sysinfo']):
      for child_idx in range(len(power_strip_info['system']['get_sysinfo']['children'])):
        plug = power_strip_info['system']['get_sysinfo']['children'][child_idx]
        unique_str = plug['alias'] + str(child_idx) + ip_address
        if not (unique_str in printed_plugs):
          print("'%s', Plug %i at %s" % ( plug['alias'], child_idx, ip_address))
          printed_plugs.append(unique_str)

def main(argv):
  global optStatus
  global optDrive
  global optList
  global ip_address
  global plug_number
  global plug_cmd_state

  parse_args(argv)
#  print('')
#  print("IP:   "   + ip_address)
#  print("Plug: " + plug_number)
#  print('')

  if (optList):
    for scan_attempt in range(5):
      try:
        if (len(ip_address) > 0):
          list_devices(ip_address)
        else:
          kasa_devices = asyncio.run(Discover.discover())
          for ip_address, kasa_dev in kasa_devices.items():
            list_devices(ip_address)
      except:
        continue
    return
  
  if (len(ip_address) == 0 or plug_number == -1):
    print_usage()
    sys.exit(2)

  for attempt in range(5):
    try:
      power_strip = SmartPowerStrip(ip_address)
      power_strip_info = power_strip.get_system_info()
      plug = power_strip_info['system']['get_sysinfo']['children'][plug_number]
      plug_state = "on" if plug['state'] else "off"

      if (optDrive):
        if (plug_state != plug_cmd_state):
          power_strip.toggle_plug(plug_cmd_state, plug_name=plug['alias'])
        else:
          print("success")
          return

      if (optStatus):
        print(plug_state)
        return

    except:
      continue
  print("error")

if __name__ == "__main__":
  main(sys.argv[1:])
