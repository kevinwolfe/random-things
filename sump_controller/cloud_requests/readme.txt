Cloud access:

  Following: http://itnerd.space/2017/06/19/how-to-authenticate-to-tp-link-cloud-api/
  Request application token:

    curl -H 'Content-Type: application/json' -d @./token_req.json -X POST https://wap.tplinkcloud.com -o token_response.json

  Device list request:

     curl -X POST -H "Content-Type: text/plain" --data "{\"method\":\"getDeviceList\"}" https://wap.tplinkcloud.com?token=68cc8e62-ATMvsqkH0cFaXYfgiFvGJI0 -o device_list.json

  Device information:

    curl -H 'Content-Type: application/json' -d @./device_info_req.json -X POST https://wap.tplinkcloud.com?token=68cc8e62-ATMvsqkH0cFaXYfgiFvGJI0  -o device_info_response.json
    
  Plug information:
    
    curl -H 'Content-Type: application/json' -d @./device_plug_info_req.json -X POST https://wap.tplinkcloud.com?token=68cc8e62-ATMvsqkH0cFaXYfgiFvGJI0  -o device_plug_info_response.json
    
    
Local access:

  pip install python-kasa
  
  Discover devices:
    /home/kevin/.local/bin/kasa
    
  Connect directly:
  
    /home/kevin/.local/bin/kasa --host 192.168.0.220 --strip
    /home/kevin/.local/bin/kasa --host 192.168.0.220 --plug 0
  
  