#include <stdlib.h>
#include <time.h>
#include <mqtt.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdbool.h>        // For kbhit()
#include <stdio.h>          // For kbhit()
#include <string.h>         // For kbhit()
#include <termio.h>         // For kbhit()
#include <unistd.h>         // For kbhit() & I2C port
#include <fcntl.h>				  // Needed for I2C port
#include <sys/ioctl.h>			// Needed for I2C port
#include <regex.h>

#include "templates/posix_sockets.h"
#include "parson.h"

#if ( 1 )
  #include <linux/i2c-dev.h>	// Needed for I2C port
  #include <wiringPi.h>
#else
  void wiringPiSetup() { };
  #define I2C_SLAVE   0
  #define INPUT       0
  #define OUTPUT      0
  int digitalRead( int ) { return 0; }
  void digitalWrite( int, int ) { }
  void pinMode( int, int ) { }
#endif

#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))

#define msleep( delay_ms )          usleep( delay_ms * 1000 )
#define SECONDS_TO_MS( seconds )    ( seconds * 1000 )
#define MINUTES_TO_MS( minutes )    ( SECONDS_TO_MS( minutes * 60 ) )

#define ARRAY_SIZE(x) ((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x])))))

// Rasp pi wiring #s are confusing as hell: http://wiringpi.com/pins/
static const uint8_t        pin_optical_sensor         =  4;    //  4 = IO23 on the breakout board.
static const uint8_t        pin_dt_level_sensor_signal =  0;    //  0 = IO17 on the breakout board.
static const uint8_t        pin_dt_level_sensor_vcc    =  1;    //  1 = IO18 on the breakout board.
static const uint8_t        pin_qt_level_sensor_signal = 25;    // 25 = IO26 on the breakout board.
static const uint8_t        pin_qt_level_sensor_vcc    = 23;    // 23 = IO13 on the breakout board.


static bool                 exit_program            = false;

static struct mqtt_client_t mqtt_client             = { 0 };
static bool                 mqtt_init_complete      = false;

static const uint32_t ato_overfill_duration_before_close_s  =  5;      // Amount of time needed for a wet sensor before the solenoid opens
static const uint32_t ato_underfill_duration_before_open_s  = 15;      // Debounced sensor time before turning pump on
static const uint32_t solenoid_max_open_time_s              =  3 * 60;      // Maximum amount of time to keep the solenoid energized for
static const uint32_t solenoid_min_cooldown_time_s          = 15 * 60;      // Minimum amount of time between solenoid energize periods

static const uint32_t wc_min_wet_period_before_pump_on_s  = 5;
static const uint32_t wc_min_dry_period_before_pump_off_s = 5;

// NOTE: Raspberry PI Relay hat: NC & NO are BACKWARDS!!!!
#define RELAY_ENERGIZE    1
#define RELAY_RELEASE     0

typedef enum
{
  // I2C addr 0x10 = top board
  // I2C addr 0x11 = bottom board
  // < I2C Address > < Local address >
  RELAY_IDX_QT_ATO_SOLENOID_12V   = 0x1001,    // IDX1 = relay above power plug (closest to Power LED)
  RELAY_IDX_DT_ATO_SOLENOID_12V   = 0x1002,
  RELAY_IDX_GND_PORTS1_DO_NOT_USE = 0x1003,
  RELAY_IDX_GND_PORTS2_DO_NOT_USE = 0x1004,    // IDX4 = relay above audio jack (closest to ethernet port)
  RELAY_IDX_DT_WC_PUMP            = 0x1101,
  RELAY_IDX_QT_WC_PUMP            = 0x1102,
  RELAY_IDX_GND_PORTS3_DO_NOT_USE = 0x1103,
  RELAY_IDX_UNUSED                = 0x1104,
} relay_idx_t;


typedef enum
{
  DISPLAY_TANK,
  QUARANTINE_TANK,
  TANK_CNT,
} tank_id_t;


//-----------------------------------------------------------------------------
typedef struct
{
  uint8_t   pin_signal;
  uint8_t   pin_vcc;
  double    low_point;
  uint32_t  low_point_charge_period_us;
  double    high_point;
  uint32_t  high_point_charge_period_us;
} etape_sensor_t;

etape_sensor_t etape_sensors[] = 
{
  [DISPLAY_TANK] =
  {
    .pin_signal                   = pin_dt_level_sensor_signal,
    .pin_vcc                      = pin_dt_level_sensor_vcc,
    .low_point                    = 0,
    .low_point_charge_period_us   = 230000,
    .high_point                   = 15,          // cm
    .high_point_charge_period_us  = 135000,
  },
  [QUARANTINE_TANK] =
  {
    .pin_signal                   = pin_qt_level_sensor_signal,
    .pin_vcc                      = pin_qt_level_sensor_vcc,
//    .low_point                    = 0,
//    .low_point_charge_period_us   = 203000,
    .low_point                    = 0,
    .low_point_charge_period_us   = 230000,

    .high_point                   = 15,          // cm
    .high_point_charge_period_us  = 135000,
  },
};

#define DISPLAY_TANK_TARGET_WATER_LEVEL      20.0
#define QUARANTINE_TANK_TARGET_WATER_LEVEL   14.0

static double           s_water_levels[TANK_CNT] = { 0 };
static pthread_mutex_t  s_state_mutex;

//-----------------------------------------------------------------------------
typedef enum
{
  PLUG_STATE_OFF,
  PLUG_STATE_ON,
  PLUG_STATE_INVALID,
} plug_state_t;

typedef enum
{
  PUMP_OFF = 0,
  PUMP_ON  = 1,
} pump_state_t;

//-----------------------------------------------------------------------------
static void       mqtt_reconnect_client(struct mqtt_client_t* client, void **reconnect_state_vptr);
static bool       set_relay_state( uint16_t relay_idx, bool close_relay );
static uint64_t   get_time_us( void );
static uint64_t   get_time_ms( void );
static bool       kbhit();
static void       set_pump_state( relay_idx_t pump_relay, pump_state_t pump_state );
static void       open_fill_solenoid( tank_id_t tank_id );
static void       close_fill_solenoid( tank_id_t tank_id );
static bool       debounce_sensor_input( uint8_t pin );
static void       update_pin_io_values( void );
static void       * level_sensors_thread_func( void* arg );
static void       * water_change_pump_control_thread_func( void* arg );
static void       * ato_control_thread_func( void* arg );
static void       * mqtt_thread_func( void* arg );
static void       * mqtt_update_thread_func( void* arg );

static void       mqtt_msg_handler_water_change( const char *msg );
static void       mqtt_msg_handler_manual_control( const char *msg );

void mqtt_subscribe_callback(void** unused, struct mqtt_response_publish *published);


typedef struct
{
  const char *topic;
  void (*handler)(const char *message);
} mqtt_subscriptions_t;

static const mqtt_subscriptions_t mqtt_subscriptions[] =
{
  { .topic = "reef/water_change",   .handler = mqtt_msg_handler_water_change },
  { .topic = "reef/manual_control", .handler = mqtt_msg_handler_manual_control },
};

typedef struct
{
  bool      is_open;
  uint64_t  last_open_time_ms;
  uint64_t  last_close_time_ms;
  uint64_t  scheduled_open_time_ms;
  uint64_t  scheduled_close_time_ms;
  bool      initial_open;
} solenoid_state_t;

typedef struct
{
  bool    ato_active;
  double  target_water_level;
} ato_state_t;

typedef struct
{
  bool              optical_sensor_wet;
  pump_state_t      dt_wc_pump_state;
  pump_state_t      qt_wc_pump_state;
  solenoid_state_t  solenoids[TANK_CNT];
  ato_state_t       ato[TANK_CNT];
  bool              dt_in_water_change_mode;
  bool              qt_in_water_change_mode;

  bool      water_change_request_start;
  bool      water_change_request_stop;
  uint16_t  water_change_request_duration_s;

  bool      manual_control_enabled;
  bool      manual_control_dt_wc_pump_on;
  bool      manual_control_qt_wc_pump_on;
  bool      manual_control_open_solenoid_request[TANK_CNT];
} states_t;

static states_t state = { 0 };

//-----------------------------------------------------------------------------
void print( const char *fmt, ... )
{
  uint64_t now = get_time_ms();
  va_list args;
  va_start(args, fmt);
  printf( "[%06lli.%02lli] ", now / 1000, ( now % 1000 ) / 10 );
  vprintf(fmt, args);
  va_end(args);
}

//-----------------------------------------------------------------------------
// Returns true on error
static bool set_relay_state( uint16_t relay_idx, bool close_relay )
{
	int i2c_fd = open( "/dev/i2c-1", O_RDWR );
	if ( i2c_fd < 0)
	{
		return true;
	}

	int     i2c_addr   = ( relay_idx >> 8 ) & 0xFF;
  uint8_t local_addr = ( relay_idx & 0xFF );
	if ( ioctl( i2c_fd, I2C_SLAVE, i2c_addr ) < 0 )
	{
    goto exit;
	}

	unsigned char buffer[2] = { local_addr, close_relay ? 0xFF : 0x00 };
	if ( write( i2c_fd, buffer, sizeof( buffer ) ) != sizeof( buffer ) )
	{
		print("Failed to write to the i2c bus.\n");
	}

exit:

  close( i2c_fd );
  return false;
}

//-----------------------------------------------------------------------------
// Returns true on errors
static void * mqtt_thread_func( void* arg )
{
  mqtt_init_reconnect( &mqtt_client, mqtt_reconnect_client, NULL, mqtt_subscribe_callback );
  mqtt_init_complete = true;
  while( !exit_program )
  {
    mqtt_sync( &mqtt_client );
    msleep( 100 );
  }

  return NULL;
}

//-----------------------------------------------------------------------------
void mqtt_reconnect_client(struct mqtt_client_t* client, void **reconnect_state_vptr)
{
  static uint8_t sendbuf[2048];
  static uint8_t recvbuf[1024];

  static bool first_msg        = true;
  static bool mqtt_connected   = false;

  bool mqtt_was_connected      = mqtt_connected;
  mqtt_connected = false;

  close( client->socketfd );

  // Open a new socket
  int socketfd = open_nb_socket( "192.168.0.41", "1883" );
  if (socketfd == -1)
  {
    goto exit;
  }

  // Reinitialize the client
  mqtt_reinit( client, socketfd,  sendbuf, sizeof( sendbuf ), recvbuf, sizeof( recvbuf ) );
  mqtt_connect( client, NULL, NULL, NULL, 0, NULL, NULL, MQTT_CONNECT_CLEAN_SESSION, 400);
  mqtt_sync( client );
  mqtt_connected = ( client->error == MQTT_OK );

exit:
  if ( mqtt_connected )
  {
    for ( uint8_t idx = 0; idx < ARRAY_SIZE( mqtt_subscriptions ); idx++ )
    {
      mqtt_subscribe( client, mqtt_subscriptions[idx].topic, 0);
    }
  }
  else
  {
    close( socketfd );
  }

  if ( first_msg || ( mqtt_connected != mqtt_was_connected ) )
  {
    print( "MQTT %sconnected!\n", mqtt_connected ? "" : "dis" );
    first_msg = false;
  }
}

//-----------------------------------------------------------------------------
void mqtt_subscribe_callback( void** unused, struct mqtt_response_publish *published )
{
  // Note: published->topic_name is NOT null-terminated
  for ( uint8_t idx = 0; idx < ARRAY_SIZE( mqtt_subscriptions ); idx++ )
  {
    if ( !memcmp( published->topic_name, mqtt_subscriptions[idx].topic, strlen( mqtt_subscriptions[idx].topic ) ) )
    {
      if ( mqtt_subscriptions[idx].handler )
      {
        mqtt_subscriptions[idx].handler( published->application_message );
      }
    }
  }
}

//-----------------------------------------------------------------------------
static void       mqtt_msg_handler_water_change( const char *msg )
{
  // Example input: {"state":"stop", "duration_s":0}
  JSON_Value  * root_value  = NULL;
  JSON_Object * root_object = NULL;

  root_value =  json_parse_string( msg );
  root_object = json_value_get_object( root_value );
  if ( !json_object_has_value_of_type( root_object, "state", JSONString ) || !json_object_has_value_of_type( root_object, "duration_s", JSONNumber ) )
  {
    goto func_exit;
  }

  state.water_change_request_start      = !strcmp( json_object_get_string( root_object, "state" ), "start" );
  state.water_change_request_stop       = !strcmp( json_object_get_string( root_object, "state" ), "stop" );
  state.water_change_request_duration_s = json_object_get_number( root_object, "duration_s" );

func_exit:
  if ( root_value )
  {
    json_value_free(root_value);
  }
}

//-----------------------------------------------------------------------------
static void       mqtt_msg_handler_manual_control( const char *msg )
{
  // manual_control = {"manual_control":"off", "pump_on":"off", "dt_solenoid_open":"off", "qt_solenoid_open":"off"}
  JSON_Value  * root_value  = NULL;
  JSON_Object * root_object = NULL;

  printf( "Manual Control JSON Message in: %s\n", msg );

  root_value =  json_parse_string( msg );
  root_object = json_value_get_object( root_value );
  if ( !json_object_has_value_of_type( root_object, "manual_control", JSONString ) ||
       !json_object_has_value_of_type( root_object, "dt_wc_pump_on", JSONString ) ||
       !json_object_has_value_of_type( root_object, "qt_wc_pump_on", JSONString ) ||
       !json_object_has_value_of_type( root_object, "open_dt_solenoid", JSONString ) ||
       !json_object_has_value_of_type( root_object, "open_qt_solenoid", JSONString ) )
  {
    goto func_exit;
  }

  // Anything but an "on" value will be treated as an "off" value
  state.manual_control_enabled           = !strcmp( json_object_get_string( root_object, "manual_control" ), "on" );
  state.manual_control_dt_wc_pump_on     = !strcmp( json_object_get_string( root_object, "dt_wc_pump_on" ), "on" );
  state.manual_control_qt_wc_pump_on     = !strcmp( json_object_get_string( root_object, "qt_wc_pump_on" ), "on" );
  state.manual_control_open_solenoid_request[DISPLAY_TANK]    = !strcmp( json_object_get_string( root_object, "open_dt_solenoid" ), "on" );
  state.manual_control_open_solenoid_request[QUARANTINE_TANK] = !strcmp( json_object_get_string( root_object, "open_qt_solenoid" ), "on" );

  print( "Manual control message: enabled: %i, dt wc pump on: %i, qt wc pumps on: %i/%i, dt_solenoid on: %i, qt_solenoid on: %i\n",
          state.manual_control_enabled,
          state.manual_control_dt_wc_pump_on,
          state.manual_control_qt_wc_pump_on,
          state.manual_control_open_solenoid_request[DISPLAY_TANK],
          state.manual_control_open_solenoid_request[QUARANTINE_TANK] );

func_exit:
  if ( root_value )
  {
    json_value_free(root_value);
  }
}

//-----------------------------------------------------------------------------
uint64_t get_time_ms( void )
{
  return get_time_us() / 1000;
}


//-----------------------------------------------------------------------------
uint64_t get_time_us( void )
{
  static uint64_t start_time_us = 0;
  struct timespec now;
  clock_gettime( CLOCK_MONOTONIC, &now);

  uint64_t now_us = (((uint64_t)now.tv_sec) * ((uint64_t)1000000)) + (((uint64_t)now.tv_nsec) / ((uint64_t)1000));
  if ( start_time_us == 0 )
  {
    start_time_us = now_us;
  }

  return ( now_us - start_time_us );
}

//-----------------------------------------------------------------------------
static bool kbhit(void)
{
    struct termios original;
    tcgetattr(STDIN_FILENO, &original);
    struct termios term;
    memcpy(&term, &original, sizeof(term));
    term.c_lflag &= ~ICANON;
    tcsetattr(STDIN_FILENO, TCSANOW, &term);
    int characters_buffered = 0;
    ioctl(STDIN_FILENO, FIONREAD, &characters_buffered);
    tcsetattr(STDIN_FILENO, TCSANOW, &original);
    bool pressed = (characters_buffered != 0);
    return pressed;
}

//-----------------------------------------------------------------------------
static void set_pump_state( relay_idx_t pump_relay, pump_state_t pump_state )
{
  print( "Turning %s Water Change pump #%x\n", ( pump_state == PUMP_ON ) ? "on" : "off", pump_relay );
  set_relay_state( pump_relay, ( pump_state == PUMP_ON ) ? RELAY_ENERGIZE : RELAY_RELEASE );

  // TODO: Verify result
  if ( pump_relay == RELAY_IDX_DT_WC_PUMP )
  {
    state.dt_wc_pump_state = pump_state;
  }
  if ( pump_relay == RELAY_IDX_QT_WC_PUMP )
  {
    state.qt_wc_pump_state = pump_state;
  }
}

//-----------------------------------------------------------------------------
static void open_fill_solenoid( tank_id_t tank_id )
{
  print( "Opening solenoid %i\n", tank_id );
//  if ( tank_id != QUARANTINE_TANK )
  {
    set_relay_state( ( tank_id == DISPLAY_TANK ? RELAY_IDX_DT_ATO_SOLENOID_12V : RELAY_IDX_QT_ATO_SOLENOID_12V ), RELAY_ENERGIZE );
  }
  state.solenoids[tank_id].is_open           = true;   // TODO: Verify result
  state.solenoids[tank_id].last_open_time_ms = get_time_ms();
}

//-----------------------------------------------------------------------------
static void close_fill_solenoid( tank_id_t tank_id )
{
  print( "Closing solenoid %i\n", tank_id );
  set_relay_state( ( tank_id == DISPLAY_TANK ? RELAY_IDX_DT_ATO_SOLENOID_12V : RELAY_IDX_QT_ATO_SOLENOID_12V ), RELAY_RELEASE );
  state.solenoids[tank_id].is_open            = false;    // TODO: Verify result
  state.solenoids[tank_id].last_close_time_ms = get_time_ms();
}

//-----------------------------------------------------------------------------
static bool debounce_sensor_input( uint8_t pin )
{
  bool    last_read            = digitalRead( pin );
  uint8_t consistent_value_cnt = 0;

  for ( uint16_t idx = 0; idx < 1000; idx++ )
  {
    bool new_read = digitalRead( pin );
    if ( new_read == last_read )
    {
      if ( ++consistent_value_cnt == 20 )   // 20mSec debounce
      {
        return new_read;
      }
    }
    else
    {
      consistent_value_cnt = 0;
    }

    last_read = new_read;
    msleep( 1 );
  }

  print( "ERROR DEBOUNCING SENSOR PIN: %i!!!\n", pin );
  return false;
}

//-----------------------------------------------------------------------------
static void update_pin_io_values( void )
{
  static bool _pins_init = false;
  if ( !_pins_init )
  {
    wiringPiSetup();
    pinMode(pin_optical_sensor, INPUT);
    pinMode(pin_dt_level_sensor_signal, INPUT);
    pinMode(pin_dt_level_sensor_vcc, OUTPUT);
    pinMode(pin_qt_level_sensor_signal, INPUT);
    pinMode(pin_qt_level_sensor_vcc, OUTPUT);

    _pins_init = true;
  }

  state.optical_sensor_wet       = !debounce_sensor_input( pin_optical_sensor );   // water state = reverse polarity
}

//-----------------------------------------------------------------------------
static void * level_sensors_thread_func( void* arg )
{
  #define         READINGS_TO_AVERAGE   ( 10 )
  #define         MAX_READINGS_STDDEV   ( 0.5 )
  const uint16_t  discharge_time_ms   = 500;
  
  double  plot_fit_slope[TANK_CNT];
  double  plot_fit_intercept[TANK_CNT];
  double  level_readings[TANK_CNT][READINGS_TO_AVERAGE] = { 0 };
  uint8_t level_readings_idx = 0;
  static uint32_t total_readings = 0;

  // Init
  for ( uint8_t tank_id = 0; tank_id < TANK_CNT; tank_id++ )
  {
    etape_sensor_t *p_sensor = &etape_sensors[tank_id];
    digitalWrite( etape_sensors[tank_id].pin_vcc, 0 );
    plot_fit_slope[tank_id]     = ( p_sensor->high_point - p_sensor->low_point ) / ( (double)p_sensor->high_point_charge_period_us - p_sensor->low_point_charge_period_us );
    plot_fit_intercept[tank_id] = p_sensor->high_point - ( p_sensor->high_point_charge_period_us * plot_fit_slope[tank_id] );
  }

  // Discharge period
  msleep( discharge_time_ms );

  // Main loop
  while ( !exit_program )
  {
    total_readings++;
    for ( uint8_t tank_id = 0; tank_id < TANK_CNT; tank_id++ )
    {
      etape_sensor_t *p_sensor = &etape_sensors[tank_id];

      uint64_t start_timestamp_us = get_time_us();
      digitalWrite( p_sensor->pin_vcc, 1 );

      uint16_t timeout_ms = 1500;
      while ( !digitalRead( p_sensor->pin_signal ) && ( get_time_us() < ( start_timestamp_us + ( timeout_ms * 1000 ) ) ) )
      {}

      uint64_t charge_period_us = get_time_us() - start_timestamp_us;
      level_readings[tank_id][level_readings_idx] = charge_period_us; //( charge_period_us * plot_fit_slope[tank_id] ) + plot_fit_intercept[tank_id];
      
      double readings_mean   = 0;
      double readings_stddev = 0;
      for ( uint8_t reading_idx = 0; reading_idx < READINGS_TO_AVERAGE; reading_idx++)
      {
          readings_mean += level_readings[tank_id][reading_idx];
      }
      readings_mean /= READINGS_TO_AVERAGE;
/*      for ( uint8_t reading_idx = 0; reading_idx < READINGS_TO_AVERAGE; reading_idx++)
      {
          readings_stddev += pow( level_readings[tank_id][reading_idx] - readings_mean, 2 );
      }
      readings_stddev = sqrt(readings_stddev / READINGS_TO_AVERAGE);
*/
      
      double mean_water_level = ( readings_mean * plot_fit_slope[tank_id] ) + plot_fit_intercept[tank_id];
      pthread_mutex_lock( &s_state_mutex );
      s_water_levels[tank_id] = ( total_readings > READINGS_TO_AVERAGE ? mean_water_level : 0 );
      pthread_mutex_unlock( &s_state_mutex );

      printf( "%i: avg: %.2f, %.0f, ", tank_id, s_water_levels[tank_id], readings_mean );

      digitalWrite( p_sensor->pin_vcc, 0 );
    }
    
    level_readings_idx++;
    level_readings_idx %= READINGS_TO_AVERAGE;
    printf("\n");

    // Discharge period
    msleep( discharge_time_ms );
  }

  digitalWrite( pin_dt_level_sensor_vcc, 0 );

  return NULL;
}


//-----------------------------------------------------------------------------
static void * water_change_pump_control_thread_func( void* arg )
{
  uint64_t pump_scheduled_turn_on_time_ms  = 0;
  uint64_t pump_scheduled_turn_off_time_ms = 0;

  print("Water Change Pump control thread starting\n");

  set_pump_state( RELAY_IDX_DT_WC_PUMP, PUMP_OFF );
  set_pump_state( RELAY_IDX_QT_WC_PUMP, PUMP_OFF );

  while ( !exit_program )
  {
    msleep( 50 );
    uint64_t now_ms = get_time_ms();

    if ( state.manual_control_enabled )
    {
      (  state.manual_control_dt_wc_pump_on && ( state.dt_wc_pump_state != PUMP_ON  ) ) ? set_pump_state( RELAY_IDX_DT_WC_PUMP, PUMP_ON )  : NULL;
      ( !state.manual_control_dt_wc_pump_on && ( state.dt_wc_pump_state != PUMP_OFF ) ) ? set_pump_state( RELAY_IDX_DT_WC_PUMP, PUMP_OFF ) : NULL;
      (  state.manual_control_qt_wc_pump_on && ( state.qt_wc_pump_state != PUMP_ON  ) ) ? set_pump_state( RELAY_IDX_QT_WC_PUMP, PUMP_ON )  : NULL;
      ( !state.manual_control_qt_wc_pump_on && ( state.qt_wc_pump_state != PUMP_OFF ) ) ? set_pump_state( RELAY_IDX_QT_WC_PUMP, PUMP_OFF ) : NULL;

      continue;
    }

    // Only control the pump when we're running in water change mode
    if ( !state.dt_in_water_change_mode )
    {
      ( state.dt_wc_pump_state != PUMP_OFF ) ? set_pump_state( RELAY_IDX_DT_WC_PUMP, PUMP_OFF) : NULL;
      pump_scheduled_turn_on_time_ms = 0;
      pump_scheduled_turn_off_time_ms = 0;
      continue;
    }

    // Running in water change mode, control the pump
    if ( state.optical_sensor_wet )
    {
      if ( pump_scheduled_turn_off_time_ms )
      {
        printf( "Canceling pump turn off\n" );
        pump_scheduled_turn_off_time_ms = 0;
      }

      if ( state.dt_wc_pump_state != PUMP_ON )
      {
        if ( pump_scheduled_turn_on_time_ms == 0 )
        {
          pump_scheduled_turn_on_time_ms = now_ms + ( wc_min_wet_period_before_pump_on_s * 1000 );
          print( "Turning on pump at: %li.%02li\n", pump_scheduled_turn_on_time_ms / 1000, ( pump_scheduled_turn_on_time_ms % 1000 ) / 10 );
        }
        if ( now_ms > pump_scheduled_turn_on_time_ms )
        {
          set_pump_state( RELAY_IDX_DT_WC_PUMP, PUMP_ON );
          pump_scheduled_turn_on_time_ms = 0;
        }
      }
    }
    else // !state.optical_sensor_wet
    {
      if ( pump_scheduled_turn_on_time_ms )
      {
        printf( "Canceling pump turn on\n" );
        pump_scheduled_turn_on_time_ms = 0;
      }

      if ( state.dt_wc_pump_state != PUMP_OFF )
      {
        if ( pump_scheduled_turn_off_time_ms == 0 )
        {
          pump_scheduled_turn_off_time_ms = now_ms + ( wc_min_dry_period_before_pump_off_s * 1000 );
          print( "Turning off pump at: %li.%02li\n", pump_scheduled_turn_off_time_ms / 1000, ( pump_scheduled_turn_off_time_ms % 1000 ) / 10 );
        }

        if ( now_ms > pump_scheduled_turn_off_time_ms )
        {
          set_pump_state( RELAY_IDX_DT_WC_PUMP, PUMP_OFF );
          pump_scheduled_turn_off_time_ms = 0;
        }
      }
    }
  }

  set_pump_state( RELAY_IDX_DT_WC_PUMP, PUMP_OFF );
  set_pump_state( RELAY_IDX_QT_WC_PUMP, PUMP_OFF );

  return NULL;
}

//-----------------------------------------------------------------------------
static void close_all_solenoids()
{
  for ( tank_id_t tank_id = 0; tank_id < TANK_CNT; tank_id++ )
  {
    close_fill_solenoid( tank_id );
  }
}

//-----------------------------------------------------------------------------
static void * ato_control_thread_func( void* arg )
{
  uint64_t water_change_state_end_time_ms   = 0;

  state.solenoids[DISPLAY_TANK].initial_open      = true;
  state.solenoids[QUARANTINE_TANK].initial_open   = true;

  state.ato[DISPLAY_TANK].target_water_level      = DISPLAY_TANK_TARGET_WATER_LEVEL;
  state.ato[QUARANTINE_TANK].target_water_level   = QUARANTINE_TANK_TARGET_WATER_LEVEL;

  double min_valid_water_level_reading            = 5.0;

  close_all_solenoids();

  while ( !exit_program )
  {
    msleep( 50 );

    uint64_t now_ms               = get_time_ms();

    if ( state.manual_control_enabled )
    {
      for ( tank_id_t tank_id = 0; tank_id < TANK_CNT; tank_id++ )
      {
        (  state.manual_control_open_solenoid_request[tank_id] && !state.solenoids[tank_id].is_open ) ? open_fill_solenoid( tank_id ) : NULL;
        ( !state.manual_control_open_solenoid_request[tank_id] &&  state.solenoids[tank_id].is_open ) ? close_fill_solenoid( tank_id ) : NULL;
      }

      continue;
    }

    for ( tank_id_t tank_id = 0; tank_id < TANK_CNT; tank_id++ )
    {
      if ( !state.ato[tank_id].ato_active )
      {
        state.solenoids[tank_id].is_open ? close_fill_solenoid( tank_id ) : NULL;
        state.solenoids[tank_id].scheduled_open_time_ms  = 0;
        state.solenoids[tank_id].scheduled_close_time_ms = 0;
        continue;
      }

      // Running in automatic top off mode, control the dt_solenoid
      pthread_mutex_lock( &s_state_mutex );
      double water_level = s_water_levels[tank_id];
      pthread_mutex_unlock( &s_state_mutex );

      if ( water_level < min_valid_water_level_reading )
      {
        continue;
      }
      
      bool optical_sensor_wet = state.optical_sensor_wet && ( tank_id == DISPLAY_TANK );

      if ( optical_sensor_wet || ( water_level >= state.ato[tank_id].target_water_level ) )
      {
        if ( state.solenoids[tank_id].scheduled_open_time_ms )
        {
          print( "Canceling scheduled open for %i\n", tank_id );
          state.solenoids[tank_id].scheduled_open_time_ms = 0;
        }

        if ( state.solenoids[tank_id].is_open && !state.solenoids[tank_id].scheduled_close_time_ms )
        {
          state.solenoids[tank_id].scheduled_close_time_ms = now_ms + ( ato_overfill_duration_before_close_s * 1000 );
          print( "solenoid %i close scheduled for: %f\n", tank_id, (double)state.solenoids[tank_id].scheduled_close_time_ms / 1000 );
        }

        if ( state.solenoids[tank_id].is_open && ( now_ms > state.solenoids[tank_id].scheduled_close_time_ms ) )
        {
          printf("Closing solenid %i\n", tank_id );
          close_fill_solenoid( tank_id );
          state.solenoids[tank_id].scheduled_close_time_ms = 0;
        }
      }
      else // !state.optical_sensor_wet
      {
        if ( state.solenoids[tank_id].is_open && ( now_ms > ( state.solenoids[tank_id].last_open_time_ms + ( solenoid_max_open_time_s * 1000 ) ) ) )
        {
          print( "ERROR: Max solenoid %i open time limit hit!\n", tank_id );
          close_fill_solenoid( tank_id );
          state.solenoids[tank_id].scheduled_close_time_ms = 0;
        }

        if ( !state.solenoids[tank_id].is_open && !state.solenoids[tank_id].scheduled_open_time_ms )
        {
          uint32_t cooldown_time_s = ( state.solenoids[tank_id].initial_open ? 10 : solenoid_min_cooldown_time_s );       // 10 second delay on start
          state.solenoids[tank_id].scheduled_open_time_ms = MAX(  now_ms + ( ato_underfill_duration_before_open_s * 1000 ), state.solenoids[tank_id].last_close_time_ms + ( cooldown_time_s * 1000 ) );          
          
          print( "solenoid %i open scheduled for: %f\n\n", tank_id, ( double )state.solenoids[tank_id].scheduled_open_time_ms / 1000 );
        }

        if ( !state.solenoids[tank_id].is_open && ( now_ms > state.solenoids[tank_id].scheduled_open_time_ms ) )
        {
          printf("Opening solenid %i\n", tank_id );
          state.solenoids[tank_id].scheduled_open_time_ms = 0;
          open_fill_solenoid( tank_id );
          state.solenoids[tank_id].initial_open = false;
        }
      }
    }
  }

  close_all_solenoids();

  return NULL;
}

//-----------------------------------------------------------------------------
static void * mqtt_update_thread_func( void* arg )
{
  while ( !exit_program )
  {
    static uint32_t message_id = 0;
    if ( mqtt_init_complete )
    {
      char unknown_str[] = "Unknown";

      // Note: Home Assistant only works with messages <= 255 chars in length
      char msg[256];

#define BOOL_TO_STRING( val )             ( val ? "True" : "False" )
#define OPTICAL_SENSOR_TO_STRING( val )   ( val ? "Wet" : "Dry" )
#define SOLENOID_STATE_TO_STRING( val )   ( val ? "Open" : "Closed" )
#define PUMP_STATE_TO_STRING( val )       ( ( val == PUMP_ON ) ? "Running" : "Idle" )
#define WC_STATE_TO_STRING( val )         ( val ? "Yes" : "No" )

      pthread_mutex_lock( &s_state_mutex );
      sprintf( msg,
              "{\n"
                "\"message_id\":\"%i\",\n"
                "\"manual_control\":\"%s\",\n"
                "\"optical_sensor\":\"%s\",\n"
                "\"dt_water_level\":%.2f,\n"
                "\"qt_water_level\":%.2f,\n"
                "\"dt_solenoid\":\"%s\",\n"
                "\"qt_solenoid\":\"%s\"\n"
              "}\n",
              message_id,
              BOOL_TO_STRING( state.manual_control_enabled ),
              OPTICAL_SENSOR_TO_STRING( state.optical_sensor_wet ),
              s_water_levels[DISPLAY_TANK],
              s_water_levels[QUARANTINE_TANK],
              SOLENOID_STATE_TO_STRING( state.solenoids[DISPLAY_TANK].is_open ),
              SOLENOID_STATE_TO_STRING( state.solenoids[QUARANTINE_TANK].is_open ) );
      pthread_mutex_unlock( &s_state_mutex );

      mqtt_publish( &mqtt_client, "reef/ato/status", msg, strlen(msg), MQTT_PUBLISH_QOS_1 );

      pthread_mutex_lock( &s_state_mutex );
      sprintf( msg,
              "{\n"
                "\"message_id\":\"%i\",\n"
                "\"manual_control\":\"%s\",\n"
                "\"dt_wc_pump\":\"%s\",\n"
                "\"qt_wc_pump\":\"%s\",\n"
                "\"dt_in_wc\":\"%s\",\n"
                "\"qt_in_wc\":\"%s\"\n"
              "}\n",
              message_id,
              BOOL_TO_STRING( state.manual_control_enabled ),
              PUMP_STATE_TO_STRING( state.dt_wc_pump_state ),
              PUMP_STATE_TO_STRING( state.qt_wc_pump_state ),
              WC_STATE_TO_STRING( state.dt_in_water_change_mode ) ,
              WC_STATE_TO_STRING( state.qt_in_water_change_mode ) );
      pthread_mutex_unlock( &s_state_mutex );

      mqtt_publish( &mqtt_client, "reef/water_change/status", msg, strlen(msg), MQTT_PUBLISH_QOS_1 );

      message_id++;
    }

    msleep( 3000 );
  }

  return NULL;
}

//-----------------------------------------------------------------------------
int main(int argc, const char *argv[])
{
  print( "Reef ATO Controller app running!\n" );

  update_pin_io_values(); // Call this first to init all pins

  pthread_t mqtt_thread = (pthread_t)NULL;
  pthread_create( &mqtt_thread, NULL, mqtt_thread_func, NULL );

  pthread_t fill_solenoids_control_thread = (pthread_t)NULL;
  pthread_create( &fill_solenoids_control_thread, NULL, ato_control_thread_func, NULL );

  pthread_t water_change_pump_control_thread = (pthread_t)NULL;
  pthread_create( &water_change_pump_control_thread, NULL, water_change_pump_control_thread_func, NULL );

  pthread_t level_sensors_thread = (pthread_t)NULL;
  pthread_create( &level_sensors_thread, NULL, level_sensors_thread_func, NULL );

  pthread_t mqtt_update_thread = (pthread_t)NULL;
  pthread_create( &mqtt_update_thread, NULL, mqtt_update_thread_func, NULL );

  uint64_t water_change_mode_end_time_ms = 0;

  while ( !kbhit() )
  {
    msleep( 50 );

    uint64_t now_ms = get_time_ms();
    update_pin_io_values();

    if ( state.water_change_request_start )
    {
      water_change_mode_end_time_ms         = now_ms + ( state.water_change_request_duration_s * 1000 );
      state.water_change_request_start      = false;
      state.water_change_request_duration_s = 0;
      print( "Water Change mode starting, ending at: %li.%02li\n", water_change_mode_end_time_ms / 1000, ( water_change_mode_end_time_ms % 1000 ) / 10 );
    }
    if ( state.water_change_request_stop && water_change_mode_end_time_ms )
    {
      water_change_mode_end_time_ms   = 0;
      state.water_change_request_stop = false;
      print( "Water Change mode stopping\n" );
    }

    state.dt_in_water_change_mode = ( now_ms < water_change_mode_end_time_ms );
    state.ato[DISPLAY_TANK].ato_active    = !state.dt_in_water_change_mode;
    state.ato[QUARANTINE_TANK].ato_active = true;
  }

  print( "\n" );
  print( "Key press detected, stopping activities\n" );

  exit_program =  true;

  pthread_join( fill_solenoids_control_thread, NULL );
  pthread_join( water_change_pump_control_thread, NULL );
  pthread_join( level_sensors_thread, NULL );
  pthread_join( mqtt_update_thread, NULL );
  pthread_join( mqtt_thread, NULL );

  print( "All threads finished, exiting.\n" );
}
